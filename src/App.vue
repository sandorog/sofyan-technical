<template>
  <main class="content todo">
    <div class="todo__container">
      <h2 class="title">To-Do List</h2>

      <section class="todo__form">
        <form @submit.prevent="addTodo">
          <h3>Enter To-Do</h3>
          <div class="todo__inputs">
            <input type="text" placeholder="To-Do" v-model="text" />
            <input type="submit" value="Add todo" />
          </div>
        </form>
      </section>

      <section class="todo__todos">
        <h3 v-show="todos.length === 0">To-Do List is empty</h3>

        <div v-show="todos.length > 0" class="todo__filter">
          <button
            v-for="(filter, index) in filters"
            :key="index"
            :class="{ active: activeFilter === filter }"
            @click="setFilter(filter)"
          >
            {{ filter }}
          </button>
        </div>
        <div class="todo__list">
          <div v-for="todo in filteredTodos" :class="`todo__item ${todo.done}`">
            <label class="todo__checkbox">
              <input type="checkbox" v-model="todo.done" />
              <span class="todo__checkmark"></span>
            </label>

            <div class="todo__content">
              <input type="text" v-model="todo.todo" />
            </div>

            <div class="todo__actions">
              <button class="todo__delete" @click="deleteTodo(todo)">
                Delete
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  </main>
</template>

<script setup>
import { ref, onMounted, watch, computed } from 'vue';

const todos = ref([]);
const text = ref('');
const filters = ref(['All', 'Done', 'ToDo']);
const activeFilter = ref('All');

const filteredTodos = computed(() => {
  if (activeFilter.value === 'All') {
    return todos.value;
  }
  if (activeFilter.value === 'Done') {
    return todos.value.filter((item) => item.done);
  }
  if (activeFilter.value === 'ToDo') {
    return todos.value.filter((item) => !item.done);
  }
});

function addTodo() {
  if (text.value.trim() === '') {
    return;
  }

  todos.value.unshift({
    todo: text.value,
    done: false,
  });

  text.value = '';
}

function deleteTodo(todo) {
  todos.value = todos.value.filter((item) => item !== todo);
}

function setFilter(state) {
  activeFilter.value = state;
}

watch(
  todos,
  (newTodoValue) => {
    localStorage.setItem('todos', JSON.stringify(newTodoValue));
  },
  { deep: true }
);

onMounted(() => {
  todos.value = JSON.parse(localStorage.getItem('todos')) || [];
});
</script>

<style lang="scss" scoped>
@import './styles/vars';
@import './styles/mixins';

.todo {
  &__container {
    padding: 0 280px;

    @include mobile {
      padding: 0 20px;
    }
  }

  &__form {
    margin-bottom: 86px;
  }

  &__inputs {
    display: flex;
    align-items: center;

    input[type='submit'] {
      margin-left: 32px;
    }
  }

  &__todos {
    padding-top: 28px;
    border-top: 1px solid $color-mischka;
  }

  &__item {
    display: flex;
    justify-items: center;
    align-items: center;
    padding: 8px 0;
  }

  &__content {
    flex: 1;
  }

  &__checkbox {
    display: block;
    position: relative;
    padding-left: 36px;
    margin-bottom: 24px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;

      &:checked ~ .todo__checkmark {
        background-color: $color-dodger-blue;

        &:after {
          display: block;
        }
      }
    }

    &:hover input ~ .todo__checkmark {
      background-color: $color-silver;
    }
  }

  &__checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    border-radius: 0.2rem;
    background-color: $color-gallery;

    &:after {
      content: '';
      position: absolute;
      display: none;
      left: 9px;
      top: 5px;
      width: 5px;
      height: 10px;
      border: solid #fff;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
    }
  }

  &__filter {
    display: flex;
    margin-bottom: 24px;

    button {
      margin-right: 12px;

      &.active {
        background-color: $color-dodger-blue;
        color: #fff;

        &:hover {
          background-color: lighten($color-dodger-blue, 10%);
        }
      }
    }
  }

  input[type='text'] {
    width: 100%;
    padding: 0.75rem 1rem;
    background: $color-alabaster;
    border: 1px solid transparent;
    color: $color-cod-gray;
    border-radius: 0.5rem;
    font-size: 1rem;
    box-sizing: border-box;
  }

  button,
  input[type='submit'] {
    background-color: #fff;
    border: 0;
    border-radius: 0.5rem;
    font-size: 0.875rem;
    font-weight: 600;
    line-height: 1.25rem;
    padding: 0.75rem 1rem;
    text-align: center;
    text-decoration: none $color-mischka solid;
    text-decoration-thickness: auto;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
    user-select: none;
    -webkit-user-select: none;
    touch-action: manipulation;
    cursor: pointer;

    &:hover {
      background-color: rgb(249, 250, 251);
    }

    &:focus {
      outline: 2px solid transparent;
      outline-offset: 2px;
    }

    &.todo__delete {
      margin-left: 12px;
      background-color: $color-persimmon;
      color: #fff;
    }
  }
}
</style>
